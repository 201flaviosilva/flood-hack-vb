﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Flood_Heack_Spam_
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Mensagem = New System.Windows.Forms.TextBox()
        Me.Intervalo = New System.Windows.Forms.Button()
        Me.Nmensagens = New System.Windows.Forms.Button()
        Me.Iniciar = New System.Windows.Forms.Button()
        Me.Sair = New System.Windows.Forms.Button()
        Me.Parar = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Timer1
        '
        Me.Timer1.Interval = 1
        '
        'Mensagem
        '
        Me.Mensagem.Font = New System.Drawing.Font("Consolas", 15.75!, System.Drawing.FontStyle.Bold)
        Me.Mensagem.Location = New System.Drawing.Point(12, 46)
        Me.Mensagem.Name = "Mensagem"
        Me.Mensagem.Size = New System.Drawing.Size(379, 32)
        Me.Mensagem.TabIndex = 0
        '
        'Intervalo
        '
        Me.Intervalo.Font = New System.Drawing.Font("Consolas", 15.75!, System.Drawing.FontStyle.Bold)
        Me.Intervalo.Location = New System.Drawing.Point(12, 139)
        Me.Intervalo.Name = "Intervalo"
        Me.Intervalo.Size = New System.Drawing.Size(128, 86)
        Me.Intervalo.TabIndex = 1
        Me.Intervalo.Text = "Intervalo"
        Me.Intervalo.UseVisualStyleBackColor = True
        '
        'Nmensagens
        '
        Me.Nmensagens.Font = New System.Drawing.Font("Consolas", 15.75!, System.Drawing.FontStyle.Bold)
        Me.Nmensagens.Location = New System.Drawing.Point(264, 139)
        Me.Nmensagens.Name = "Nmensagens"
        Me.Nmensagens.Size = New System.Drawing.Size(127, 86)
        Me.Nmensagens.TabIndex = 2
        Me.Nmensagens.Text = "Número de Mensagens"
        Me.Nmensagens.UseVisualStyleBackColor = True
        '
        'Iniciar
        '
        Me.Iniciar.Enabled = False
        Me.Iniciar.Font = New System.Drawing.Font("Consolas", 15.75!, System.Drawing.FontStyle.Bold)
        Me.Iniciar.Location = New System.Drawing.Point(12, 231)
        Me.Iniciar.Name = "Iniciar"
        Me.Iniciar.Size = New System.Drawing.Size(128, 86)
        Me.Iniciar.TabIndex = 3
        Me.Iniciar.Text = "Iniciar"
        Me.Iniciar.UseVisualStyleBackColor = True
        '
        'Sair
        '
        Me.Sair.Font = New System.Drawing.Font("Consolas", 15.75!, System.Drawing.FontStyle.Bold)
        Me.Sair.Location = New System.Drawing.Point(146, 139)
        Me.Sair.Name = "Sair"
        Me.Sair.Size = New System.Drawing.Size(112, 178)
        Me.Sair.TabIndex = 4
        Me.Sair.Text = "Sair"
        Me.Sair.UseVisualStyleBackColor = True
        '
        'Parar
        '
        Me.Parar.Font = New System.Drawing.Font("Consolas", 15.75!, System.Drawing.FontStyle.Bold)
        Me.Parar.Location = New System.Drawing.Point(264, 231)
        Me.Parar.Name = "Parar"
        Me.Parar.Size = New System.Drawing.Size(127, 86)
        Me.Parar.TabIndex = 5
        Me.Parar.Text = "Parar"
        Me.Parar.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label1.Font = New System.Drawing.Font("Consolas", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(379, 34)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Mensagem:"
        '
        'Label2
        '
        Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label2.Font = New System.Drawing.Font("Consolas", 15.75!, System.Drawing.FontStyle.Bold)
        Me.Label2.Location = New System.Drawing.Point(264, 85)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(127, 51)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "0000"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Timer2
        '
        Me.Timer2.Interval = 1000
        '
        'Label3
        '
        Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label3.Font = New System.Drawing.Font("Consolas", 15.75!, System.Drawing.FontStyle.Bold)
        Me.Label3.Location = New System.Drawing.Point(12, 85)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(246, 51)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "Número de Mensagens:"
        '
        'Label4
        '
        Me.Label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label4.Font = New System.Drawing.Font("Consolas", 15.75!, System.Drawing.FontStyle.Bold)
        Me.Label4.Location = New System.Drawing.Point(12, 320)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(246, 51)
        Me.Label4.TabIndex = 10
        Me.Label4.Text = "Segundos para Começar o Spam:"
        '
        'Label5
        '
        Me.Label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label5.Font = New System.Drawing.Font("Consolas", 15.75!, System.Drawing.FontStyle.Bold)
        Me.Label5.Location = New System.Drawing.Point(264, 320)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(127, 51)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "0000"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Flood_Heack_Spam_
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(398, 377)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Parar)
        Me.Controls.Add(Me.Sair)
        Me.Controls.Add(Me.Iniciar)
        Me.Controls.Add(Me.Nmensagens)
        Me.Controls.Add(Me.Intervalo)
        Me.Controls.Add(Me.Mensagem)
        Me.Name = "Flood_Heack_Spam_"
        Me.Text = "Spam"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Mensagem As System.Windows.Forms.TextBox
    Friend WithEvents Intervalo As System.Windows.Forms.Button
    Friend WithEvents Nmensagens As System.Windows.Forms.Button
    Friend WithEvents Iniciar As System.Windows.Forms.Button
    Friend WithEvents Sair As System.Windows.Forms.Button
    Friend WithEvents Parar As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Timer2 As System.Windows.Forms.Timer
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
End Class
