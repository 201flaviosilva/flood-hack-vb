﻿Public Class Flood_Heack_Spam_
    Dim intr, nmens, tempos, comecar As Double
    'inter= intervalo entre as mensagens'
    'nmens= Número de Mensagens'
    'rotacoes = Número de tempos'

    Private Sub Intervalo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Intervalo.Click
        intr = InputBox("Tempo entre as mensagens", "Informação")
        Timer1.Interval = intr
    End Sub

    Private Sub Nmensagens_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Nmensagens.Click
        nmens = InputBox("Número de mensagens", "Informação")
        Label2.Text = nmens
        Iniciar.Enabled = True
    End Sub

    Private Sub Inicial_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Iniciar.Click
        Timer2.Enabled = True
        comecar = 3
        Label5.Text = comecar
    End Sub

    Private Sub Parar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Parar.Click
        Timer1.Enabled = False
        Timer1.Stop()
    End Sub

    Private Sub Sair_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Sair.Click
        End
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        tempos = tempos + 1
        Label3.Text = "Número de Mensagens Enviadas:"
        Label2.Text = tempos & "/" & nmens

        If tempos = nmens Or tempos > nmens Then
            Timer1.Enabled = False
            Timer1.Stop()
        End If

        SendKeys.Send(Mensagem.Text)
        SendKeys.Send("{Enter}")
    End Sub

    Private Sub Timer2_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer2.Tick
        comecar = comecar - 1
        Label5.Text = comecar

        If comecar = 0 Then
            Timer2.Enabled = False

            Timer1.Enabled = True
            Timer1.Start()
            tempos = 0
        End If

    End Sub
End Class